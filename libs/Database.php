<?php
class Database extends PDO
{
    public function __construct()
    {
        try {
            //code...
            parent::__construct(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
        } catch (PDOException  $e) {
            echo "Error: ".$e;
        }
        // header('Content-Type: application/json');
        // Create connection
        // $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        // $this->conn->set_charset("utf8");
    }
}
