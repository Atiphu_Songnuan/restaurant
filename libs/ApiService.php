<?php
class ApiService
{
    public function __construct()
    {

    }

    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    public function GetAllPaymentByShopID()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GetAllPaymentByShopID($request->shopid);
    }

    public function SessionDestroy()
    {
        // session_unset();
        session_destroy();
    }
}
