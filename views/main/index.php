<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Restaurant</title>
    
    <link rel="icon" type="image/png" href="./public/images/icons/shop.ico"/>
    <link href="./public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="./public/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Select2 -->
    <link rel="stylesheet" href="./public/vendor/select2/dist/css/select2.min.css">
    
    <!-- Morris charts -->
    <link rel="stylesheet" href="./public/vendor/morris.js/morris.css">
    
    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
            background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);
            font-family: 'Kanit', serif;
        }
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            vertical-align: middle;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
		    font-size: 0.7rem !important;
	    }
    </style>
</head>
<body class="wrapper">

    <!-- Require Header from header.php -->
    <?php require './views/header.php'?>

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <!-- <h4 class="h3 mb-0 text-gray-800">ผลประกอบการ</h4> -->
        </div>

        <div class="row">
            <!-- Card 1 -->
            <div class="col-xl-3 col-md-6 mb-4">
                <select class="form-control select2" id="shopselected">
                    <option value="" disabled selected>เลือกร้านค้า</option>
                    <?php 
                        $shopjsondata = json_decode($this->GetAllShop, true);
                        foreach ($shopjsondata as $value) {
                            echo '<option value='.$value[0].'>'.$value[1].'</option>';
                        }
                    ?>
                    <!--                 
                    <option value="1">1</option>
                    <option value="1">1</option>
                    <option value="1">1</option> -->
                </select>
            </div>
            <!-- End Card 1 --> 
        </div>

        <div class="row">
            <!-- Card 1 -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <label class="text-xs font-weight-bold text-danger text-uppercase mb-1" id="shopname" style="font-size: 20px">ชื่อร้านค้า</label>
                                <br>
                                <label class="text-xs font-weight-bold text-primary text-uppercase mb-1" style="font-size: 20px">ยอดขายรวม</label>
                                <br>
                                <label class="h5 mb-0 font-weight-bold text-gray-800" id="total">0 ฿</label>
                            </div>
                            <div class="col-auto">
                                <i class="fab fa-btc fa-4x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card 1 --> 
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header">ประวัติการขาย</h5>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="payment" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>เวลา</th>
                                    <th>บุคลากร</th>
                                    <th>ราคา</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content Row -->

    </div>
</div>
    <!-- Bootstrap core JavaScript-->
    <script src="./public/vendor/jquery/dist/jquery.min.js"></script>
    <script src="./public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="./public/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="./public/js/sb-admin-2.min.js"></script>

    <!-- SweetAlert2 -->
    <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <!-- Page level plugin JavaScript-->
	<script src="./public/vendor/datatables/jquery.dataTables.js"></script>
	<script src="./public/vendor/datatables/dataTables.bootstrap4.js"></script>
	<!-- Demo scripts for this page-->
	<script src="./public/js/demo/datatables-demo.js"></script>

    <!-- Select2 -->
    <script src="./public/vendor/select2/dist/js/select2.full.min.js"></script>

    <script>
        $(function(){
            $('.select2').select2();

            var table = $('#payment').dataTable({
                "order": [[ 0, "desc" ]],
                "aLengthMenu": [5, 10],
                "iDisplayLength": 5,
                "columnDefs": [
                    { 
                        "targets": 2,
                        render: function(data, type, row){
                            return '<span class="badge badge-success">'+data+'</span>'
                        }
                    },

                    {
                        "targets": 2,
                        "class": "text-center"
                    },

                    {
                        "targets": 1,
                        render: function(data, type, row){
                            return '<p class="text-info">'+ data +'</p>'
                        }
                    }
                ]
                // "columnDefs": [
                //     {
                //         "targets": [ 0 ],
                //         "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                //         "searchable": false
                //     }
                // ]
            });


            // var table = $('#payment').DataTable();
            // table.row.add([
            //     jsondata['paymentdate'],
            //     jsondata['perid'],
            //     jsondata['name'] + " " + jsondata['surname'],
            //     jsondata['food'],
            //     jsondata['price']
            // ]).draw();
        });

        $('#shopselected').on('change', function(){
            var jsondata = {'shopid': $('#shopselected').val()};
            $('#shopname').text("ร้าน" + $('#shopselected option:selected').text());
            $.ajax({
                type:"POST",
                url:"../restaurant/ApiService/GetAllPaymentByShopID",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(paymentresponse) {
                    // console.log(paymentresponse[0]['FOOD_NAME']);
                    var table = $('#payment').DataTable();
                    var total = 0;
                    table.clear().draw();


                    paymentresponse.forEach(element => {
                        // console.log(element);
                        total += parseInt(element['PAYMENT_MONEY']);

                            table.row.add([
                                element['PAYMENT_DATE'],
                                element['PERID'] + " " + element['NAME'] + " " + element['SURNAME'],
                                element['PAYMENT_MONEY']
                        ]).draw();
                    });

                    $('#total').text(total + " ฿");
                },
                error: function(response){
                    console.log(response);
                }
            });
        });

    </script>
   
</body>
</html>