<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Restaurant</title>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="./public/images/icons/shop.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./public/vendor/login/bootstrap-login/css/bootstrap.min.css">
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="./public/fonts/font-awesome-4.7.0/css/font-awesome.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./public/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./public/css/util.css">
	<link rel="stylesheet" type="text/css" href="./public/css/main.css">
<!--===============================================================================================-->

 <!-- SweetAlert2 -->
 <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">

 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
        font-family: 'Kanit', serif;
        font-size: 28px;
        }
        .swal2-popup {
		font-size: 0.7rem !important;
	    }
    </style>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
						Restaurant
					</span>
					<span class="login100-form-title p-b-48">
						<i class="zmdi zmdi-store"></i>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "กรอก username ให้ถูกต้อง">
						<input class="input100" type="text" id="loginusername" autocomplete="off">
						<span class="focus-input100" data-placeholder="ชื่อผู้ใช้งาน"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="กรอก password ให้ถูกต้อง">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" id="loginpassword" autocomplete="off">
						<span class="focus-input100" data-placeholder="รหัสผ่าน"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type="button" class="login100-form-btn" id="btnlogin" onclick="Login()">ลงชื่อเข้าใช้งาน</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="#">
							Sign Up
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="dropDownSelect1"></div>
	
	<!--===============================================================================================-->
	<script src="./public/vendor/login/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="./public/vendor/login/bootstrap-login/js/popper.js"></script>
	<!--===============================================================================================-->
	<script src="./public/vendor/login/bootstrap-login/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="./public/js/main.js"></script>

<!-- SweetAlert2 -->
<script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

<script>
	function Login(){
		var loginData = {"username": $('#loginusername').val(),
						"password": $('#loginpassword').val()};
		// console.log(loginData);
		$.ajax({
			type: "POST",
			url: "../restaurant/ApiService/Login",
			header:{
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: JSON.stringify(loginData),
			contentType: "application/json",
			dataType: "json",
			success: function(response){
				if (response.length != 0) {
					Swal.fire({
						position: 'center',
						type: 'success',
						title: 'เข้าสู่ระบบสำเร็จ',
						showConfirmButton: false,
						timer: 1500,
						onClose: () =>{
						window.location.href='../restaurant/main';
						}
					});	
				} else{
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Username หรือ Password ไม่ถูกต้อง, โปรด login ใหม่',
						showConfirmButton: false,
						timer: 1500,
					});
				}
			},
			error: function(response){
				console.log("Login Failed: " + response);
			}
		});
	}

	$('body').keyup(function(e) {
		// console.log('keyup called');
		var code = e.keyCode || e.which;
		if (code == '13') {
		Login();
		}
  	});
</script>
</body>
</html>