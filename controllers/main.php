<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
        $this->views->GetAllShop = $this->model->GetAllShop();
        // $this->views->ExeFileDataByID = $this->model->GetAllDataByID($_SESSION['PERID']);
    }

    public function index()
    {
        $this->views->render('main/index');
    }

}
