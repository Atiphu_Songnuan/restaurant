<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAllShop()
    {
        $sql = 'SELECT SHOP_ID, SHOP_NAME  FROM tbl_shop';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        return $jsonData;
    }

    public function GetAllPaymentByShopID($shopid)
    {
        $sql = "SELECT PAYMENT_ID, PERID, NAME, SURNAME, FOOD_NAME, SHOP_NAME, PAYMENT_MONEY, PAYMENT_DATE FROM viewpayment WHERE  SHOP_ID=$shopid";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

}
